# Security Policies and Procedures

This document outlines security procedures and general security policies for this repo.

  * [Security Related Information](#security-info)
  * [Reporting a Bug](#reporting-a-bug)
  * [Disclosure Policy](#disclosure-policy)
  * [Comments on this Policy](#comments-on-this-policy)

## Security Related Information

** < Describe general high importance security information about the project>. Example of a thorough running securely information section : https://github.com/apache/storm/blob/master/SECURITY.md **

## Reporting a Bug or Security Vulnerability

Pearson takes all security bugs in application code seriously. Report security bugs by 
emailing the lead maintainer at ** <project-lead-maintainer@pearson.com> **.

The team will respond indicating the next steps in handling
your report. After the initial reply to your report, the security team will
endeavor to keep you informed of the progress towards a fix and full
announcement, and may ask for additional information or guidance.

Report security bugs in third-party modules to the person or team maintaining
the module.

## Disclosure Policy

When the security team receives a security bug report, they will assign it to a
primary handler. This person will coordinate the fix and release process,
involving the following steps:

  * Confirm the problem and determine the affected versions.
  * Audit code to find any potential similar problems.
  * Prepare fixes for all releases still under maintenance. These fixes will be
    released as fast as possible to npm.

## Comments on this Policy

If you have suggestions on how this process could be improved please submit a
pull request.

For more info please see Pearson product security office knowledge base at https://one-confluence.pearson.com/display/PSO/GIT+repository+secrets+management
